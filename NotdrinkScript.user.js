// ==UserScript==
// @name         NotdrinkScript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://notdrink.ru/*
// @grant        none
// ==/UserScript==

'use strict';
(function() {
    function GM_addStyle(css) {
        const style = document.getElementById("GM_addStyleBy8626") || (function() {
            const style = document.createElement('style');
            style.type = 'text/css';
            style.id = "GM_addStyleBy8626";
            document.head.appendChild(style);
            return style;
        })();
        const sheet = style.sheet;
        sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
    }

    GM_addStyle('body { font-size: 19px !important;}');
    GM_addStyle('button { font-size: 19px !important;}');
})();

window.onload = () => {
    var target = document.querySelector('body');
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if (mutation.addedNodes.length > 0) {
                if (document.querySelector('.tooltip-content')) {
                    var tooltipSpan = document.querySelector('.tooltip-content').firstChild;
                    var reply = document.querySelector('.actionBar-action--reply'),
                        mq = document.querySelector('.actionBar-action--mq');

                    var newReply = document.createElement('button');
                    newReply.innerHTML = 'Цитата';
                    var href = reply.getAttribute("href");
                    newReply.setAttribute("data-message-id", tooltipSpan.childNodes[0].getAttribute("data-message-id"));
                    newReply.setAttribute("data-mq-action", "add");
                    newReply.setAttribute("onclick", `/${href}`);
                    newReply.setAttribute("class", "u-jsOnly js-multiQuote");
                    var newMq = document.createElement('button');
                    newMq.innerHTML = 'Ответить';
                    newMq.setAttribute("data-quote-href", tooltipSpan.childNodes[2].getAttribute("data-quote-href"));
                    newMq.setAttribute('data-xf-click', "quote");
                    href = mq.getAttribute("href");
                    newMq.setAttribute("onclick", `/${href}`);

                    tooltipSpan.childNodes[0].parentNode.replaceChild(newReply, tooltipSpan.childNodes[0]);
                    tooltipSpan.childNodes[0].parentNode.replaceChild(newMq, tooltipSpan.childNodes[2]);
                    //                     tooltipSpan.childNodes[0].parentNode.removeChild(tooltipSpan.childNodes[1]);
                }
            }
        });
    });
    var config = { attributes: true, childList: true, characterData: true }
    observer.observe(target, config);
};